package br.gov.rj.seplag.dados;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class OracleDAL {

    private Connection c;
    private boolean isConnected = false;

    
    //desenv
    //private final String userName = "PORTAL_MOBILE";
    //private final String userName = "sys";
    //private final String passName = "qwti@2016";
    //private final String url = "jdbc:oracle:thin:@192.168.151.6:1521:xe"; 
    
/* 
    //homologação / produção
    private final String userName = "portal_mobile";
    private final String passName = "mobile2016";
    private final String url = "jdbc:oracle:thin:@10.9.200.34:1521:db11g"; 
    
     //desenvolvimento seplag
    private String userName = "portal_adm";
    private String passName = "portal2015";
    private String url = "jdbc:oracle:thin:@10.9.200.34:1521:db11g";
 
*/
    
    public boolean isConnected() {
        return this.isConnected;
    }

    public void connect() throws Exception {

        try {
            Context initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:/datasources/govrio");
            this.c = ds.getConnection();
            
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            //this.c = DriverManager.getConnection(url, userName, passName);
            
            isConnected = true;
       
        } catch (Exception e) {
            isConnected = false;
            throw e;
        }
    }

    public boolean disconnect() throws Exception {

        try {
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            //this.c = DriverManager.getConnection(url, userName, passName);
            this.c.close();
            isConnected = false;
            
        } catch (Exception e) {
            isConnected = false;
            throw e;       
        }

        return isConnected;
    }

    public ResultSet executarQuery(String query) throws Exception {

        if (c == null) {
            throw new Exception("O objeto de conexão com o banco está nulo.");
        }
        if (c.isClosed()) {
            throw new Exception("A conexão com o banco está fechada.");
        }
              
        return this.c.prepareStatement(query).executeQuery(query);
    }
}