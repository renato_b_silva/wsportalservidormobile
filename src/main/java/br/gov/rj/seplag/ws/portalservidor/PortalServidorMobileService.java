package br.gov.rj.seplag.ws.portalservidor;

import br.gov.rj.seplag.model.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService(name = "wSPortalServidorMobile")
public interface PortalServidorMobileService {

    @WebMethod(operationName = "ObterListaResumoContracheque", action = "ObterListaResumoContracheque")
    public @WebResult(name = "RetornoListaResumoContracheque")
    RetornoListaResumoContracheque ObterListaResumoContracheque(@WebParam(name = "cpf") String cpf);

    @WebMethod(operationName = "ObterContracheque", action = "ObterContracheque")
    public @WebResult(name = "RetornoContracheque")
    RetornoContracheque ObterContracheque(@WebParam(name = "cpf") String cpf,
                                          @WebParam(name = "codigoUnicoFolha") long codigoUnicoFolha);

    @WebMethod(operationName = "ObterUltimoContracheque", action = "ObterUltimoContracheque")
    public @WebResult(name = "RetornoContracheque") RetornoContracheque ObterUltimoContracheque(@WebParam(name = "cpf") String cpf);

    @WebMethod(operationName = "ObterListaResumoInformeRendimentos", action = "ObterListaResumoInformeRendimentos")
    public @WebResult(name = "RetornoListaResumoInformeRendimentos")
    RetornoListaResumoInformeRendimentos ObterListaResumoInformeRendimentos(@WebParam(name = "cpf") String cpf);

    @WebMethod(operationName = "ObterInformeRendimentos", action = "ObterInformeRendimentos")
    public @WebResult(name = "RetornoInformeRendimentos") RetornoInformeRendimentos ObterInformeRendimentos(@WebParam(name = "numInfRendimento") int codigoInformeRendimentos);

    @WebMethod(operationName = "ValidarLogin", action = "ValidarLogin")
    public @WebResult(name = "RetornoLogin") RetornoLogin validarLogin(@WebParam(name = "login") String login,
                                                                       @WebParam(name = "senha") String senha);

    @WebMethod(operationName = "ObterUltimaFolhaContracheque", action = "ObterUltimaFolhaContracheque")
    public @WebResult(name = "RetornoUltimaFolhaContracheque") RetornoUltimaFolhaContracheque ObterUltimaFolhaContracheque();

    @WebMethod(operationName = "ObterListaUltimoContrachequeFuncionarios", action = "ObterListaUltimoContrachequeFuncionarios")
    public @WebResult(name = "RetornoListaUltimoContrachequeFuncionario") RetornoListaUltimoContrachequeFuncionario ObterListaUltimoContrachequeFuncionarios(@WebParam(name = "listaCpf") List<String> listaCpf);

}
