package br.gov.rj.seplag.ws.portalservidor;

import br.gov.rj.seplag.model.*;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

/**
 *
 * @author alalmeida
 */
@WebService(endpointInterface = "br.gov.rj.seplag.ws.portalservidor.PortalServidorMobileService", name = "WSPortalServidorMobile")
public class PortalServidorMobileServiceImpl implements  PortalServidorMobileService{

    @Override
    @WebMethod(operationName = "ObterListaResumoContracheque", action = "ObterListaResumoContracheque")
    public @WebResult(name = "RetornoListaResumoContracheque") RetornoListaResumoContracheque ObterListaResumoContracheque(@WebParam(name = "cpf") String cpf) {

        RetornoListaResumoContracheque r = new RetornoListaResumoContracheque();

        try {
            List<ResumoContracheque> listaResumoContracheque;
            listaResumoContracheque = new ArrayList<ResumoContracheque>();

            if (cpf != null && !"".equals(cpf)) {
                ResumoContracheque rcc = new ResumoContracheque();
                listaResumoContracheque = rcc.getListaResumoContracheque(cpf);
                r.setListaResumoContracheque(listaResumoContracheque);
            }
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterContracheque", action = "ObterContracheque")
    public @WebResult(name = "RetornoContracheque") RetornoContracheque ObterContracheque(@WebParam(name = "cpf") String cpf,
            @WebParam(name = "codigoUnicoFolha") long codigoUnicoFolha) {

       
        RetornoContracheque r = new RetornoContracheque();
        
        try {
            Contracheque contracheque = new Contracheque();
            
            r.setContracheque(contracheque.obterContracheque(cpf, codigoUnicoFolha));
            
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterUltimoContracheque", action = "ObterUltimoContracheque")
    public @WebResult(name = "RetornoContracheque") RetornoContracheque ObterUltimoContracheque(@WebParam(name = "cpf") String cpf) {

        RetornoContracheque r = new RetornoContracheque();

        try {
            Contracheque contracheque = new Contracheque();
            r.setContracheque(contracheque.obterUltimoContracheque(cpf));
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterListaResumoInformeRendimentos", action = "ObterListaResumoInformeRendimentos")
    public @WebResult(name = "RetornoListaResumoInformeRendimentos") RetornoListaResumoInformeRendimentos ObterListaResumoInformeRendimentos(@WebParam(name = "cpf") String cpf) {

        RetornoListaResumoInformeRendimentos r = new RetornoListaResumoInformeRendimentos();
        try {
            ResumoInformeRendimentos infRendimentos = new ResumoInformeRendimentos();
            r.setListaResumoInformeRendimentos(infRendimentos.obterLista(cpf));
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterInformeRendimentos", action = "ObterInformeRendimentos")
    public @WebResult(name = "RetornoInformeRendimentos") RetornoInformeRendimentos ObterInformeRendimentos(@WebParam(name = "numInfRendimento") int codigoInformeRendimentos) {

        RetornoInformeRendimentos r = new RetornoInformeRendimentos();
        
        try {
            InformeRendimentos infRendimentos = new InformeRendimentos();

            if (codigoInformeRendimentos > 0) {
                r.setInformeRendimentos(infRendimentos.obterInformeRendimentos(codigoInformeRendimentos));
            } else {
                r.setInformeRendimentos(infRendimentos);
            }
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ValidarLogin", action = "ValidarLogin")
    public @WebResult(name = "RetornoLogin") RetornoLogin validarLogin(@WebParam(name = "login") String login,
            @WebParam(name = "senha") String senha) {

        RetornoLogin r = new RetornoLogin();
        
        try {
            Funcionario funcRetorno = new Funcionario();
            
            funcRetorno = funcRetorno.validarLoginPorCPFouID(login, senha);
            
            r.setSituacaoLogin(funcRetorno != null);
            r.setFuncionarioRetorno(funcRetorno);
            
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterUltimaFolhaContracheque", action = "ObterUltimaFolhaContracheque")
    public @WebResult(name = "RetornoUltimaFolhaContracheque") RetornoUltimaFolhaContracheque ObterUltimaFolhaContracheque() {

       
        RetornoUltimaFolhaContracheque r = new RetornoUltimaFolhaContracheque();
        
        try {
            UltimaFolhaContracheque folha = new UltimaFolhaContracheque();
            
            r.setFolha(folha.obterUltimaFolhaContracheque());
            
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }

    @Override
    @WebMethod(operationName = "ObterListaUltimoContrachequeFuncionarios", action = "ObterListaUltimoContrachequeFuncionarios")
    public @WebResult(name = "RetornoListaUltimoContrachequeFuncionario") RetornoListaUltimoContrachequeFuncionario ObterListaUltimoContrachequeFuncionarios(@WebParam(name = "listaCpf") List<String> listaCpf) {

        RetornoListaUltimoContrachequeFuncionario r = new RetornoListaUltimoContrachequeFuncionario();

        try {
            List<UltimoContrachequeFuncionario> lista;            
            UltimoContrachequeFuncionario ucc = new UltimoContrachequeFuncionario();
            lista = ucc.getListaUltimoContrachequeFuncionarios(listaCpf);
            r.setListaUltimoContracheque(lista);
            
        } catch (Exception e) {
            r.setError(e);
        }

        return r;
    }        
}

