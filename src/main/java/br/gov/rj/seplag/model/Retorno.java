/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 *
 * @author alalmeida
 */
public class Retorno {
    
    public boolean Sucesso = true;
        public String MensagemErro;
        public String StackTrace;

        public void setError(Exception e) {
            MensagemErro = e.getMessage();
            StackTrace = getStackTraceString(e);
            Sucesso = false;
        }

        private String getStackTraceString(Throwable e) {
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            e.printStackTrace(printWriter);
            return result.toString();
        }
}
