
package br.gov.rj.seplag.model;

/**
 *
 * @author joao.dias
 */
public class RetornoContracheque extends Retorno {
    
    private Contracheque contracheque;
    
    public Contracheque getContracheque() {
        return contracheque;
    };
    
    public void setContracheque(Contracheque contracheque) {
        this.contracheque = contracheque;
    };

}
