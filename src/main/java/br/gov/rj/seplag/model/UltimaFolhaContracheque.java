package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alalmeida
 */
public class UltimaFolhaContracheque extends ResumoContracheque  {

    private int numeroFolha;
    
    public UltimaFolhaContracheque(){
        this.numeroFolha = 0;
    }
    
    public int getNumeroFolha() {
        return numeroFolha;
    }
    public void setNumeroFolha(int numeroFolha) {
        this.numeroFolha = numeroFolha;
    }   
    
    public UltimaFolhaContracheque obterUltimaFolhaContracheque() throws Exception {
        
        UltimaFolhaContracheque ultimaFolhaContracheque = new UltimaFolhaContracheque();
        OracleDAL con = new OracleDAL();
        int ano = 0;
        int mes = 0;

        try {
            con.connect();
            /*
            ResultSet rs = con.executarQuery("SELECT MAX(CONCAT(FOLHA_ANO, LPAD(FOLHA_MES, 2, '0'))) AS FOLHA FROM PORTAL_ADM.CONTRACHEQUES");
            
            if (rs.next()) {            
                ultimaFolhaContracheque.setNumeroFolha(rs.getInt("FOLHA"));                
            }
            */            
            ResultSet rs = con.executarQuery("SELECT MAX(FOLHA_ANO) AS ANO FROM PORTAL_ADM.CONTRACHEQUES");

            if (rs.next()) {
                ano = rs.getInt("ANO");                                              
            }
            
            if (ano > 0) {
                rs = con.executarQuery("SELECT MAX(FOLHA_MES) AS MES FROM PORTAL_ADM.CONTRACHEQUES WHERE FOLHA_ANO = " + ano);
                
                if(rs.next()) {
                    mes = rs.getInt("MES");                                    
                    ultimaFolhaContracheque.setNumeroFolha(Integer.parseInt(Integer.toString(ano) + String.format("%02d", mes)));
                }                                
            }
            
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }

        return ultimaFolhaContracheque;
    }

}
