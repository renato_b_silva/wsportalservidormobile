/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

import java.util.List;

/**
 *
 * @author joao.dias
 */
public class RetornoListaResumoInformeRendimentos extends Retorno {
 
    private List<ResumoInformeRendimentos> listaInforme;
    
    public List<ResumoInformeRendimentos> getListaResumoInformeRendimentos() {
        return listaInforme;
    };
    
    public void setListaResumoInformeRendimentos(List<ResumoInformeRendimentos> lista) {
        this.listaInforme = lista;
    };
}


