package br.gov.rj.seplag.model;

/**
 *
 * @author alalmeida
 */
public class DetalhesContracheque {

    public DetalhesContracheque(){}
    
    public DetalhesContracheque(String nomeCampo, String valorCampo) {
        this.nomeCampo = nomeCampo;
        this.valorCampo = valorCampo;
    }

    private String nomeCampo;
    private String valorCampo;

    public String getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public String getValorCampo() {
        return valorCampo;
    }

    public void setValorCampo(String valorCampo) {
        this.valorCampo = valorCampo;
    }
}
