package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alalmeida
 */
public class UltimoContrachequeFuncionario {
    
    protected String cpf;
    protected String idFuncional;
    protected int vinculo;
    protected String tipoContracheque;
    protected int codigoOrgao;
    protected int codigoSuborgao;
    protected String siglaOrgao;
    protected String folha;
    protected int codigoUnicoFolha;
    

    public String getCPF() {
        return this.cpf;
    }
    
    public void setCPF(String cpf) {
        this.cpf = cpf;
    }
    
    public String getIdFuncional() {
        return this.idFuncional;
    }
    
    public void setIdFuncional(String idFuncional) {
        this.idFuncional = idFuncional;
    }
    
    public int getVinculo() {
        return vinculo;
    }

    public void setVinculo(int vinculo) {
        this.vinculo = vinculo;
    }
    
    public String getTipoContracheque() {
        return tipoContracheque;
    }

    public void setTipoContracheque(String tipoContracheque) {
        this.tipoContracheque = tipoContracheque;
    }
    
    public int getCodigoOrgao() {
        return codigoOrgao;
    }

    public void setCodigoOrgao(int codigoOrgao) {
        this.codigoOrgao = codigoOrgao;
    }
    
    public int getCodigoSuborgao() {
        return codigoSuborgao;
    }

    public void setCodigoSuborgao(int codigoSuborgao) {
        this.codigoSuborgao = codigoSuborgao;
    }
    
    public String getSiglaOrgao() {
        return siglaOrgao;
    }

    public void setSiglaOrgao(String siglaOrgao) {
        this.siglaOrgao = siglaOrgao;
    }
        
    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }
    

    
    public List<UltimoContrachequeFuncionario> getListaUltimoContrachequeFuncionarios(List<String> listaCpf) throws Exception {
        
        String inCPF = "";
        OracleDAL con = new OracleDAL();
        List<UltimoContrachequeFuncionario> retorno = new ArrayList<UltimoContrachequeFuncionario>();
        
        if (listaCpf == null || listaCpf.isEmpty()){
            return retorno;
        }
        
        for(String cpf : listaCpf){
            inCPF = inCPF + cpf + ",";
        }
        
        inCPF = inCPF.substring(0, inCPF.length()-1);
        
        try {
            con.connect();
            ResultSet rs = con.executarQuery(
                "SELECT " +
                " CC.CPF        AS CPF, " +
                " CC.IDFUNC     AS ID_FUNCIONAL, " +
                " CC.NUMVINC    AS VINCULO, " +
                " CC.FOLHA_REF  AS TIPO, " +
                " CC.EMP_CODIGO AS COD_ORGAO, " +
                " CC.SUBEMP_CODIGO AS COD_SUBORGAO, " +
                " SUB.FANTASIA  AS SIGLA_ORGAO, " +
                " MAX(CONCAT(CC.FOLHA_ANO, LPAD(CC.FOLHA_MES, 2, '0'))) AS FOLHA " +
                "FROM " +
                " PORTAL_ADM.CONTRACHEQUES CC INNER JOIN " +
                " PORTAL_ADM.SUBEMPRESAS SUB ON CC.EMP_CODIGO = SUB.EMP_CODIGO AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO " +
                "WHERE " +
                " CC.CPF IN (" + inCPF  + ") " +
                "GROUP BY " +
                " CC.CPF, " +
                " CC.IDFUNC, " +
                " CC.NUMVINC, " +
                " CC.FOLHA_REF,	" +
                " CC.EMP_CODIGO, " +        
                " CC.SUBEMP_CODIGO, " +
                " SUB.FANTASIA " +
                "ORDER BY " +
                " CC.CPF");
            
            while (rs.next()) {
                UltimoContrachequeFuncionario dados = new UltimoContrachequeFuncionario();
                
                dados.setCPF(rs.getString("CPF"));
                dados.setIdFuncional(rs.getString("ID_FUNCIONAL"));
                dados.setVinculo(rs.getInt("VINCULO"));     
                dados.setTipoContracheque(rs.getString("TIPO"));
                dados.setCodigoOrgao(rs.getInt("COD_ORGAO")); 
                dados.setCodigoSuborgao(rs.getInt("COD_SUBORGAO")); 
                dados.setSiglaOrgao(rs.getString("SIGLA_ORGAO")); 
                dados.setFolha(rs.getString("FOLHA"));
                
                retorno.add(dados);              
            }
            
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }

        return retorno;
    }    
}
