/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

import java.util.List;

/**
 *
 * @author bruno.fernandes
 */
public class RetornoListaUltimoContrachequeFuncionario extends Retorno {
    
    private List<UltimoContrachequeFuncionario> lista;
    
    public List<UltimoContrachequeFuncionario> getListaUltimoContracheque() {
        return lista;
    };
    
    public void setListaUltimoContracheque(List<UltimoContrachequeFuncionario> lista) {
        this.lista = lista;
    };
}
