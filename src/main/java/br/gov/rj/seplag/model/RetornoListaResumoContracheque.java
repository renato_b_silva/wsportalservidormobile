/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

import java.util.List;

/**
 *
 * @author joao.dias
 */
public class RetornoListaResumoContracheque extends Retorno {
    
    private List<ResumoContracheque> listaResumoContracheque;
    
    public List<ResumoContracheque> getListaResumoContracheque() {
        return listaResumoContracheque;
    };
    
    public void setListaResumoContracheque(List<ResumoContracheque> lista) {
        this.listaResumoContracheque = lista;
    };
}
