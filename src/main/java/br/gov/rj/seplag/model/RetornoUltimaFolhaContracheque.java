
package br.gov.rj.seplag.model;

/**
 *
 * @author joao.dias
 */
public class RetornoUltimaFolhaContracheque extends Retorno {
    
    private UltimaFolhaContracheque folha;
    
    public UltimaFolhaContracheque getFolha() {
        return folha;
    };
    
    public void setFolha(UltimaFolhaContracheque contracheque) {
        this.folha = contracheque;
    };

}
