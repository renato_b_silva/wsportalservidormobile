package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Date;


/**
 *
 * @author alalmeida
 */
public class Funcionario {

    
    private BigDecimal idFuncional;
    private String nome;
    private String email;
    private BigDecimal cpf;
    private String telefoneCelular;
    private String nomePai;
    private String nomeMae;
    private String numeroRG;
    private Date dataEmissaoRG;
    private Date dataNascimento;
    
       
    public BigDecimal getIdFuncional() {
        return idFuncional;
    }

    public void setIdFuncional(BigDecimal idFuncional) {
        this.idFuncional = idFuncional;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getCpf() {
        return cpf;
    }

    public void setCpf(BigDecimal cpf) {
        this.cpf = cpf;
    }
    
    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getNumeroRG() {
        return numeroRG;
    }

    public void setNumeroRG(String numeroRG) {
        this.numeroRG = numeroRG;
    }

    public Date getDataEmissaoRG() {
        return dataEmissaoRG;
    }

    public void setDataEmissaoRG(Date dataEmissaoRG) {
        this.dataEmissaoRG = dataEmissaoRG;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    
    public Funcionario validarLoginPorCPFouID(String CPFouID, String senha) throws Exception {
        
        if ((CPFouID == null || CPFouID == "") && (senha == null || senha == "")) {
            return null;
        }
        
        String regex = "\\d+";
        if (!CPFouID.matches(regex)) {
            return null;
        }
        
        Funcionario fucRetorno = null;
        OracleDAL con = new OracleDAL();
        
        try {
            con.connect();
            
            ResultSet rs;
                    
            if (con.isConnected()) {
                
                //Validar por CPF
                rs = con.executarQuery("SELECT IDFUNC, CPF, NOME, EMAIL, DT_NASCIMENTO, NOME_MAE, NOME_PAI, NUM_RG, DT_EXPED_RG FROM PORTAL_ADM.FUNCIONARIO WHERE CPF = " + CPFouID + " AND LOWER(POSTERIOR) = '" + senha.toLowerCase() + "'");
                //rs = con.executarQuery("SELECT IDFUNC, CPF, NOME, EMAIL, DT_NASCIMENTO, NOME_MAE FROM PORTAL_ADM.FUNCIONARIO WHERE CPF = " + CPFouID + " AND LOWER(POSTERIOR) = '" + senha.toLowerCase() + "'");
                
                if (rs != null && rs.next()) {
                    
                    fucRetorno = new Funcionario();
                    fucRetorno.idFuncional = rs.getBigDecimal("IDFUNC");
                    fucRetorno.cpf = rs.getBigDecimal("CPF");
                    fucRetorno.email = rs.getString("EMAIL");
                    fucRetorno.nome = rs.getString("NOME");
                    fucRetorno.telefoneCelular = "";
                    
                    if (rs.getDate("DT_NASCIMENTO") != null)
                        fucRetorno.dataNascimento = rs.getDate("DT_NASCIMENTO");
                    
                    if (rs.getString("NOME_MAE") != null)
                        fucRetorno.nomeMae = rs.getString("NOME_MAE");
                    
                    
                    if (rs.getString("NOME_PAI") != null)
                        fucRetorno.nomePai = rs.getString("NOME_PAI");
                    
                    if (rs.getString("NUM_RG") != null)
                        fucRetorno.numeroRG = rs.getString("NUM_RG");
                    
                    if (rs.getDate("DT_EXPED_RG") != null)
                        fucRetorno.dataEmissaoRG = rs.getDate("DT_EXPED_RG");
                    
                    
                    return fucRetorno;
                }
                else
                {
                    //Validar por ID FUNCIONAL
                    rs = con.executarQuery("SELECT IDFUNC, CPF, NOME, EMAIL, DT_NASCIMENTO, NOME_MAE, NOME_PAI, NUM_RG, DT_EXPED_RG FROM PORTAL_ADM.FUNCIONARIO WHERE IDFUNC = " + CPFouID + " AND LOWER(POSTERIOR) = '" + senha.toLowerCase() + "'");
                
                    if (rs != null && rs.next()) {
                    
                        fucRetorno = new Funcionario();
                        fucRetorno.idFuncional = rs.getBigDecimal("IDFUNC");
                        fucRetorno.cpf = rs.getBigDecimal("CPF");
                        fucRetorno.email = rs.getString("EMAIL");
                        fucRetorno.nome = rs.getString("NOME");
                        fucRetorno.telefoneCelular = "";
                        
                        if (rs.getDate("DT_NASCIMENTO") != null)
                            fucRetorno.dataNascimento = rs.getDate("DT_NASCIMENTO");

                        if (rs.getString("NOME_MAE") != null)
                            fucRetorno.nomeMae = rs.getString("NOME_MAE");

                        if (rs.getString("NOME_PAI") != null)
                            fucRetorno.nomePai = rs.getString("NOME_PAI");

                        if (rs.getString("NUM_RG") != null)
                            fucRetorno.numeroRG = rs.getString("NUM_RG");

                        if (rs.getDate("DT_EXPED_RG") != null)
                            fucRetorno.dataEmissaoRG = rs.getDate("DT_EXPED_RG");

                        return fucRetorno;
                    }
                    
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }
        
        return fucRetorno;
    }
}