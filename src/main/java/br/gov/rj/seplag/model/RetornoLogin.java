/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

/**
 *
 * @author joao.dias
 */
public class RetornoLogin extends Retorno{
    
    private boolean situacaoLogin;
    private Funcionario funcionarioRetorno;

    public Funcionario getFuncionarioRetorno() {
        return funcionarioRetorno;
    }

    public void setFuncionarioRetorno(Funcionario funcionarioRetorno) {
        this.funcionarioRetorno = funcionarioRetorno;
    }
    
    public boolean getSituacaoLogin() {
        return situacaoLogin;
    };
    
    public void setSituacaoLogin(boolean situacaoLogin) {
        this.situacaoLogin = situacaoLogin;
    };
    
}
