package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alalmeida
 */
public class ResumoContracheque {
    protected String tipoContracheque;
    protected String orgao;
    protected String folha;
    protected long codigoUnicoFolha;
    protected int vinculo;

    public long getCodigoUnicoFolha() {
        return codigoUnicoFolha;
    }

    public void setCodigoUnicoFolha(long codigoUnicoFolha) {
        this.codigoUnicoFolha = codigoUnicoFolha;
    }
        
    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public String getTipoContracheque() {
        return tipoContracheque;
    }

    public void setTipoContracheque(String tipoContracheque) {
        this.tipoContracheque = tipoContracheque;
    }

    public String getOrgao() {
        return orgao;
    }

    public void setOrgao(String orgao) {
        this.orgao = orgao;
    }
    
    public int getVinculo() {
        return vinculo;
    }

    public void setVinculo(int vinculo) {
        this.vinculo = vinculo;
    }
    
    public List<ResumoContracheque> getListaResumoContracheque(String cpf) throws Exception {
        OracleDAL con = new OracleDAL();
        List<ResumoContracheque> retorno = new ArrayList<ResumoContracheque>();
        try {
            con.connect();
            ResultSet rs = con.executarQuery("SELECT CPF," +
                                             "  IDFUNC," +
                                             "  CONCAT(FOLHA_ANO, LPAD(FOLHA_MES, 2, '0')) AS FOLHA," +
                                             "  TIPO," +
                                             "  CODIGOUNICOFOLHA," +
                                             "  ORGAO," +
                                             "  VINCULO" +
                                             " FROM" +
                                             "  (SELECT CC.CPF," +
                                             "    CC.IDFUNC," +
                                             "    CC.FOLHA_ANO," +
                                             "    CC.FOLHA_MES," +
                                             "    CC.FOLHA_REF     AS TIPO," +
                                             "    SIG.NUM_CC_SIGRH AS CODIGOUNICOFOLHA," +
                                             "    SUB.FANTASIA     AS ORGAO, " +
                                             "    CC.NUMVINC       AS VINCULO " +
                                             "  FROM PORTAL_ADM.CONTRACHEQUES CC" +
                                             "  INNER JOIN PORTAL_ADM.CC_SIGRH SIG" +
                                             "  ON CC.CCGUID = SIG.CCGUID" +
                                             "  INNER JOIN PORTAL_ADM.SUBEMPRESAS SUB" +
                                             "  ON CC.EMP_CODIGO     = SUB.EMP_CODIGO" +
                                             "  AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO" +
                                             "  WHERE CC.CPF      = " + cpf +
                                             "  UNION" +
                                             "  SELECT CC.CPF," +
                                             "    CC.IDFUNC," +
                                             "    CC.FOLHA_ANO," +
                                             "    CC.FOLHA_MES," +
                                             "    CC.FOLHA_REF      AS TIPO," +
                                             "    SIG.NUM_CC_SAPE_D AS CODIGOUNICOFOLHA," +
                                             "    SUB.FANTASIA      AS ORGAO, " +
                                             "    CC.NUMVINC       AS VINCULO " +
                                             "  FROM PORTAL_ADM.CONTRACHEQUES CC" +
                                             "  INNER JOIN PORTAL_ADM.CC_SAPE_D SIG" +
                                             "  ON CC.CCGUID = SIG.CCGUID" +
                                             "  INNER JOIN PORTAL_ADM.SUBEMPRESAS SUB" +
                                             "  ON CC.EMP_CODIGO     = SUB.EMP_CODIGO" +
                                             "  AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO" +
                                             "  WHERE CC.CPF      = " + cpf +
                                             "  UNION" +
                                             "  SELECT CC.CPF," +
                                             "    CC.IDFUNC," +
                                             "    CC.FOLHA_ANO," +
                                             "    CC.FOLHA_MES," +
                                             "    CC.FOLHA_REF      AS TIPO," +
                                             "    SIG.NUM_CC_SAPE_I AS CODIGOUNICOFOLHA," +
                                             "    SUB.FANTASIA      AS ORGAO, " +
                                             "    CC.NUMVINC       AS VINCULO " +
                                             "  FROM PORTAL_ADM.CONTRACHEQUES CC" +
                                             "  INNER JOIN PORTAL_ADM.CC_SAPE_I SIG" +
                                             "  ON CC.CCGUID = SIG.CCGUID" +
                                             "  INNER JOIN PORTAL_ADM.SUBEMPRESAS SUB" +
                                             "  ON CC.EMP_CODIGO     = SUB.EMP_CODIGO" +
                                             "  AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO" +
                                             "  WHERE CC.CPF      = " + cpf +
                                             "  UNION" +
                                             "  SELECT CC.CPF," +
                                             "    CC.IDFUNC," +
                                             "    CC.FOLHA_ANO," +
                                             "    CC.FOLHA_MES," +
                                             "    CC.FOLHA_REF      AS TIPO," +
                                             "    SIG.NUM_CC_SAPE_P AS CODIGOUNICOFOLHA," +
                                             "    SUB.FANTASIA      AS ORGAO, " +
                                             "    CC.NUMVINC       AS VINCULO " +
                                             "  FROM PORTAL_ADM.CONTRACHEQUES CC" +
                                             "  INNER JOIN PORTAL_ADM.CC_SAPE_P SIG" +
                                             "  ON CC.CCGUID = SIG.CCGUID" +
                                             "  INNER JOIN PORTAL_ADM.SUBEMPRESAS SUB" +
                                             "  ON CC.EMP_CODIGO     = SUB.EMP_CODIGO" +
                                             "  AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO" +
                                             "  WHERE CC.CPF      = " + cpf +
                                             "  ORDER BY FOLHA_ANO DESC, FOLHA_MES DESC, TIPO DESC )" +
                                             "  where ROWNUM <= 24");
            
            while (rs.next()) {
                ResumoContracheque rContrachequeSeplag = new ResumoContracheque();
                
                rContrachequeSeplag.setFolha(rs.getString("FOLHA"));
                rContrachequeSeplag.setTipoContracheque(rs.getString("TIPO"));
                rContrachequeSeplag.setCodigoUnicoFolha(rs.getLong("CODIGOUNICOFOLHA"));
                rContrachequeSeplag.setOrgao(rs.getString("ORGAO"));               
                rContrachequeSeplag.setVinculo(rs.getInt("VINCULO"));     
                retorno.add(rContrachequeSeplag);              
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }

        return retorno;
    }    
}
