package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alalmeida
 */
public class ResumoInformeRendimentos {
    private int numInfRendimento;
    private int anoBase;
    private String razao;
    private int vinculo;
    
    /**
     * @return the numInfRendimento
     */
    public int getNumInfRendimento() {
        return numInfRendimento;
    }

    /**
     * @param numInfRendimento the numInfRendimento to set
     */
    public void setNumInfRendimento(int numInfRendimento) {
        this.numInfRendimento = numInfRendimento;
    }

    /**
     * @return the anoBase
     */
    public int getAnoBase() {
        return anoBase;
    }

    /**
     * @param anoBase the anoBase to set
     */
    public void setAnoBase(int anoBase) {
        this.anoBase = anoBase;
    }

    /**
     * @return the razao
     */
    public String getRazao() {
        return razao;
    }

    /**
     * @param razao the razao to set
     */
    public void setRazao(String razao) {
        this.razao = razao;
    }
    
     public int getVinculo() {
        return vinculo;
    }

    public void setVinculo(int vinculo) {
        this.vinculo = vinculo;
    }

    public List<ResumoInformeRendimentos> obterLista(String cpf) throws Exception {
        List<ResumoInformeRendimentos> retorno;
        retorno = new ArrayList<ResumoInformeRendimentos>();
        OracleDAL con = new OracleDAL();
        
        try {
            con.connect();
            ResultSet rs = con.executarQuery("SELECT NUM_INF_RENDIMENTO, ANOBASE, RAZAO, NUMVINC FROM (" +
                                             "   SELECT NUM_INF_RENDIMENTO, ANOBASE, RAZAO, NUMVINC " +
                                             "     FROM PORTAL_ADM.INFORME_RENDIMENTOS " +
                                             "    WHERE CPF = " + cpf + " " +
                                             "   ORDER BY ANOBASE DESC ) " +
                                             " WHERE ROWNUM <= 24");
            
            while (rs.next()) {
                ResumoInformeRendimentos resumo = new ResumoInformeRendimentos();
                resumo.setNumInfRendimento(rs.getInt("NUM_INF_RENDIMENTO"));
                resumo.setAnoBase(rs.getInt("ANOBASE"));
                resumo.setRazao(rs.getString("RAZAO"));
                resumo.setVinculo(rs.getInt("NUMVINC"));
                
                retorno.add(resumo);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }
        
        return retorno;
    }
}
