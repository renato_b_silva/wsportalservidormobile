package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alalmeida
 */
public class Contracheque extends ResumoContracheque  {

    private BigDecimal totalProvento;
    private BigDecimal totalDesconto;
    private BigDecimal totalLiquido;
    private BigDecimal valorFGTS;
    private BigDecimal baseFGTS;
    private BigDecimal basePrevid;
    private BigDecimal baseIRRF;
    private String nome;
    private String idFuncional;
    public List<ItemContracheque> Descontos;
    public List<ItemContracheque> Vantagens;
    public List<DetalhesContracheque> Detalhes;
    private BigDecimal cnpjEmpresa;
    private String nomeFantasiaEmpresa;
    private int numeroFolha;
    
    public Contracheque(){
        Descontos = new ArrayList<ItemContracheque>();
        Vantagens = new ArrayList<ItemContracheque>();
    }
    
    public List<DetalhesContracheque> getDetalhes() {
        return Detalhes;
    }

    public void setDetalhes(List<DetalhesContracheque> Detalhes) {
        this.Detalhes = Detalhes;
    }

    public BigDecimal getTotalProvento() {
        return totalProvento;
    }

    public void setTotalProvento(BigDecimal totalProvento) {
        this.totalProvento = totalProvento;
    }

    public BigDecimal getTotalDesconto() {
        return totalDesconto;
    }

    public void setTotalDesconto(BigDecimal totalDesconto) {
        this.totalDesconto = totalDesconto;
    }

    public BigDecimal getTotalLiquido() {
        return totalLiquido;
    }

    public void setTotalLiquido(BigDecimal totalLiquido) {
        this.totalLiquido = totalLiquido;
    }
    
    public List<ItemContracheque> getVantangens() {
        return Vantagens;
    }

    public void setVantagens(List<ItemContracheque> Vantagens) {
        this.Vantagens = Vantagens;
    }

    public List<ItemContracheque> getDescontos() {
        return Descontos;
    }

    public void setDescontos(List<ItemContracheque> Descontos) {
        this.Descontos = Descontos;
    }

    public BigDecimal getValorFGTS() {
        return valorFGTS;
    }

    public void setValorFGTS(BigDecimal valorFGTS) {
        this.valorFGTS = valorFGTS;
    }

    public BigDecimal getBaseFGTS() {
        return baseFGTS;
    }

    public void setBaseFGTS(BigDecimal baseFGTS) {
        this.baseFGTS = baseFGTS;
    }

    public BigDecimal getBasePrevid() {
        return basePrevid;
    }

    public void setBasePrevid(BigDecimal basePrevid) {
        this.basePrevid = basePrevid;
    }

    public BigDecimal getBaseIRRF() {
        return baseIRRF;
    }

    public void setBaseIRRF(BigDecimal baseIRRF) {
        this.baseIRRF = baseIRRF;
    }

    public String getIdFuncional() {
        return idFuncional;
    }

    public void setIdFuncional(String idFuncional) {
        this.idFuncional = idFuncional;
    }
    
     public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public BigDecimal getCNPJEmpresa() {
        return cnpjEmpresa;
    }
    public void setCNPJEmpresa(BigDecimal cnpj) {
        this.cnpjEmpresa = cnpj;
    }
    
    public String getNomeFantasiaEmpresa() {
        return nomeFantasiaEmpresa;
    }
    public void setNomeFantasiaEmpresa(String nomeFantasia) {
        this.nomeFantasiaEmpresa = nomeFantasia;
    }   
    
    public int getNumeroFolha() {
        return numeroFolha;
    }
    public void setNumeroFolha(int numeroFolha) {
        this.numeroFolha = numeroFolha;
    }   
    
    public Contracheque obterContracheque(String cpf, long codigoUnicoFolha) throws Exception {
        Contracheque contracheque = new Contracheque();
        OracleDAL con = new OracleDAL();

        try {
            con.connect();
            ResultSet rs = con.executarQuery("SELECT TOTAL_GANHOS, TOTAL_DESCONTOS, TOTAL_LIQUIDO, VALOR_FGTS, "
                    + "       VALOR_BASE_CALC_FGTS, VALOR_BASE_CALC_PREV_OFICIAL, "
                    + "       VALOR_BASE_CALC_IRRF, CC.CPF, SIG.PIS_PASEP, CC.IDFUNC, CC.NOME_SERVIDOR, "
                    + "       SIG.DT_NASCIMENTO, SIG.NUMDEPEN, SIG.DEP_SAL_FAM, "
                    + "       CONCAT(FOLHA_ANO, LPAD(FOLHA_MES, 2, '0')) AS FOLHA, "
                    + "       SIG.FOLHA_REF, SIG.NUMVINC, SUB.RAZAO AS ORIGEM, CARGO_EFETIVO, "
                    + "       SUB.FANTASIA AS ORGAO, SIG.NUM_CC_SIGRH, "
                    + "       CC.DESC_VINCULO AS TIPO_VINCULO, SIG.MATRICULA_SAPE AS MATRICULA, SIG.CARGO_EFETIVO, "
                    + "       SIG.CARGO_EFETIVO_REF, SIG.CARGO_COMISSIONADO, SIG.CARGO_COMISSIONADO_REF, "
                    + "       SIG.DT_EXERCICIO, SIG.UN_ADM_SETOR_COD as UA_SETOR, SIG.UN_ADM_SETOR_DESC as LOTACAO, "                   
                    + "       CONCAT( CONCAT( CONCAT( CONCAT(SIG.BANCO_PGTO, ' | '), SIG.AGENCIA_PGTO), ' | '), SIG.CONTA_PGTO) AS BANCO_AG_CONTA, "
                    + "       SIG.DT_APOSENTADORIA, SIG.FUND_LEGAL_APOSENTADORIA, CC.CCGUID AS CODIGO_AUTENTICACAO, "
                    + "       SUB.CNPJ, SUB.NOME AS NOME_FANTASIA, SIG.NUM_FOLHA "
                    
                    + "FROM PORTAL_ADM.CONTRACHEQUES CC "
                    + "INNER JOIN PORTAL_ADM.CC_SIGRH SIG "
                    + "ON CC.CCGUID = SIG.CCGUID "
                    + "INNER JOIN PORTAL_ADM.SUBEMPRESAS SUB "
                    + "ON CC.EMP_CODIGO     = SUB.EMP_CODIGO "
                    + "AND CC.SUBEMP_CODIGO = SUB.SUBEMP_CODIGO "
                    + "WHERE CC.CPF = " + cpf + " "
                    + "AND SIG.NUM_CC_SIGRH = " + Long.toString(codigoUnicoFolha) );

            while (rs.next()) {
                contracheque.setTotalProvento(rs.getBigDecimal("TOTAL_GANHOS"));
                contracheque.setTotalDesconto(rs.getBigDecimal("TOTAL_DESCONTOS"));
                contracheque.setTotalLiquido(rs.getBigDecimal("TOTAL_LIQUIDO"));
                contracheque.setBaseFGTS(rs.getBigDecimal("VALOR_BASE_CALC_FGTS"));
                contracheque.setBaseIRRF(rs.getBigDecimal("VALOR_BASE_CALC_IRRF"));
                contracheque.setBasePrevid(rs.getBigDecimal("VALOR_BASE_CALC_PREV_OFICIAL"));
                contracheque.setValorFGTS(rs.getBigDecimal("VALOR_FGTS"));
                contracheque.setFolha(rs.getString("FOLHA"));
                contracheque.setIdFuncional(rs.getString("IDFUNC"));
                contracheque.setOrgao(rs.getString("ORGAO"));
                contracheque.setNome(rs.getString("NOME_SERVIDOR"));
                contracheque.setVinculo(rs.getInt("NUMVINC"));
                contracheque.setCNPJEmpresa(rs.getBigDecimal("CNPJ"));
                contracheque.setNomeFantasiaEmpresa(rs.getString("NOME_FANTASIA"));
                contracheque.setNumeroFolha(rs.getInt("NUM_FOLHA"));

                List<DetalhesContracheque> detalhes = new ArrayList<DetalhesContracheque>();
                detalhes.add(new DetalhesContracheque("CPF", rs.getString("CPF")));
                detalhes.add(new DetalhesContracheque("PISPASEP", rs.getString("PIS_PASEP")));                
                detalhes.add(new DetalhesContracheque("DataNascimento", rs.getString("DT_NASCIMENTO")));
                detalhes.add(new DetalhesContracheque("NumDepIR", rs.getString("NUMDEPEN")));
                detalhes.add(new DetalhesContracheque("NumDepSalFamilia", rs.getString("DEP_SAL_FAM")));                
                detalhes.add(new DetalhesContracheque("FolhaRef", rs.getString("FOLHA_REF")));
                detalhes.add(new DetalhesContracheque("Vinculo", rs.getString("NUMVINC")));
                detalhes.add(new DetalhesContracheque("Origem", rs.getString("ORIGEM")));
                detalhes.add(new DetalhesContracheque("Cargo", rs.getString("CARGO_EFETIVO")));
                detalhes.add(new DetalhesContracheque("Ref", rs.getString("ORGAO")));
                detalhes.add(new DetalhesContracheque("NUM_CC_SIGRH", rs.getString("NUM_CC_SIGRH")));
                detalhes.add(new DetalhesContracheque("TipoVinculo", rs.getString("TIPO_VINCULO")));
                detalhes.add(new DetalhesContracheque("Matricula", rs.getString("MATRICULA")));
                detalhes.add(new DetalhesContracheque("CargoEfetivo", rs.getString("CARGO_EFETIVO")));
                detalhes.add(new DetalhesContracheque("CargoEfetivoREF", rs.getString("CARGO_EFETIVO_REF")));
                detalhes.add(new DetalhesContracheque("CargoComissionado", rs.getString("CARGO_COMISSIONADO")));
                detalhes.add(new DetalhesContracheque("CargoComissionadoREF", rs.getString("CARGO_COMISSIONADO_REF")));
                detalhes.add(new DetalhesContracheque("DtExercicio", rs.getString("DT_EXERCICIO")));
                detalhes.add(new DetalhesContracheque("UASetor", rs.getString("UA_SETOR")));
                detalhes.add(new DetalhesContracheque("Lotacao", rs.getString("LOTACAO")));
                detalhes.add(new DetalhesContracheque("BancoAgConta", rs.getString("BANCO_AG_CONTA")));
                detalhes.add(new DetalhesContracheque("DtAposentadoria", rs.getString("DT_APOSENTADORIA")));
                detalhes.add(new DetalhesContracheque("FundLegalAposentadoria", rs.getString("FUND_LEGAL_APOSENTADORIA")));
                detalhes.add(new DetalhesContracheque("CodigoAutenticacao", rs.getString("CODIGO_AUTENTICACAO")));               
                
                contracheque.setDetalhes(detalhes);
            }

            rs = con.executarQuery(" SELECT RUBRICA_COD, RUBRICA_DESC, VALOR_VANTAGEM, VALOR_DESCONTO, COMPETENCIA, COMPLEMENTO "
                    + " FROM PORTAL_ADM.CC_SIGRH_LANCAMENTOS SIG "
                    + " INNER JOIN PORTAL_ADM.CC_SIGRH SIGRH ON SIG.NUM_CC_SIGRH = SIGRH.NUM_CC_SIGRH "
                    + " INNER JOIN PORTAL_ADM.CONTRACHEQUES CC ON CC.CCGUID = SIGRH.CCGUID "
                    + " WHERE CC.CPF = " + cpf + " "
                    + " AND SIG.NUM_CC_SIGRH = " + codigoUnicoFolha);

            while (rs.next()) {
                BigDecimal vantagem = rs.getBigDecimal("VALOR_VANTAGEM");
                BigDecimal desconto = rs.getBigDecimal("VALOR_DESCONTO");

                ItemContracheque i = new ItemContracheque();
                i.setInformacao(rs.getString("RUBRICA_DESC"));
                i.setItem(rs.getString("RUBRICA_COD"));
                i.setCompetencia(rs.getString("COMPETENCIA"));
                i.setInformacaoAdicional(rs.getString("COMPLEMENTO"));
                if (vantagem == null) {
                    i.setValor(desconto);
                    contracheque.Descontos.add(i);
                } else {
                    i.setValor(vantagem);
                    contracheque.Vantagens.add(i);
                }
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();
        }

        return contracheque;
    }

    public Contracheque obterUltimoContracheque(String cpf) throws Exception {
        
    	long maxCodigoUnicoFolha = 0;
        List<ResumoContracheque> lista = getListaResumoContracheque(cpf);
        if (lista == null) {
            return null;
        }
        
        for (int i = 0; i < lista.size(); i++) {
            if(maxCodigoUnicoFolha < lista.get(i).getCodigoUnicoFolha())
                maxCodigoUnicoFolha = lista.get(i).getCodigoUnicoFolha();
        }
        
        return obterContracheque(cpf, maxCodigoUnicoFolha);
    }
}
