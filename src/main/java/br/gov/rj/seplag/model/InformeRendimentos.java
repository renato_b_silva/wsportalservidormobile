package br.gov.rj.seplag.model;

import br.gov.rj.seplag.dados.OracleDAL;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alalmeida
 */
public class InformeRendimentos extends ResumoInformeRendimentos {

    private int empCodigo;
    private int subEmpCodigo;
    private String cgc;
    private String cnpj;
    private String subEmpRazao;
    private String numFunc;
    private String numVinc;
    private String descVinculo;
    private String matriculaSape;
    private String cpf;
    private String nome;
    private String natRendimento;
    private String numPens;
    private BigDecimal renda;
    private BigDecimal previdenciaOficial;
    private BigDecimal previdenciaPrivada;
    private BigDecimal pensaoAlim;
    private BigDecimal retido;
    private BigDecimal proventosAposent;
    private BigDecimal diarias;
    private BigDecimal invalidez;
    private BigDecimal lucroPJ;
    private BigDecimal titularME;
    private BigDecimal indenizacao;
    private String outrosIsentos;
    private BigDecimal outrosRendIsentos;
    private BigDecimal salario13;
    private BigDecimal outrosRendExcl;
    private String outrosExclusivo;
    private String numProcessoRRA;
    private String qtdMesesRRA;
    private String natRendimentoRRA;
    private BigDecimal rendaRRA;
    private BigDecimal custasProcessuaisRRA;
    private BigDecimal deducaoPrevOficialRRA;
    private BigDecimal pensaoAlimenticiaRRA;
    private BigDecimal retidoRRA;
    private BigDecimal isentoMolestiaRRA;
    private String infoCompl;
    private String dtEmissao;
    private String vlrRetido13s;

    /**
     * @return the empCodigo
     */
    public int getEmpCodigo() {
        return empCodigo;
    }

    /**
     * @param empCodigo the empCodigo to set
     */
    public void setEmpCodigo(int empCodigo) {
        this.empCodigo = empCodigo;
    }

    /**
     * @return the subEmpCodigo
     */
    public int getSubEmpCodigo() {
        return subEmpCodigo;
    }

    /**
     * @param subEmpCodigo the subEmpCodigo to set
     */
    public void setSubEmpCodigo(int subEmpCodigo) {
        this.subEmpCodigo = subEmpCodigo;
    }

    /**
     * @return the cgc
     */
    public String getCgc() {
        return cgc;
    }

    /**
     * @param cgc the cgc to set
     */
    public void setCgc(String cgc) {
        this.cgc = cgc;
    }

    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * @return the subEmpRazao
     */
    public String getSubEmpRazao() {
        return subEmpRazao;
    }

    /**
     * @param subEmpRazao the subEmpRazao to set
     */
    public void setSubEmpRazao(String subEmpRazao) {
        this.subEmpRazao = subEmpRazao;
    }

    /**
     * @return the numFunc
     */
    public String getNumFunc() {
        return numFunc;
    }

    /**
     * @param numFunc the numFunc to set
     */
    public void setNumFunc(String numFunc) {
        this.numFunc = numFunc;
    }

    /**
     * @return the numVinc
     */
    public String getNumVinc() {
        return numVinc;
    }

    /**
     * @param numVinc the numVinc to set
     */
    public void setNumVinc(String numVinc) {
        this.numVinc = numVinc;
    }

    /**
     * @return the descVinculo
     */
    public String getDescVinculo() {
        return descVinculo;
    }

    /**
     * @param descVinculo the descVinculo to set
     */
    public void setDescVinculo(String descVinculo) {
        this.descVinculo = descVinculo;
    }

    /**
     * @return the matriculaSape
     */
    public String getMatriculaSape() {
        return matriculaSape;
    }

    /**
     * @param matriculaSape the matriculaSape to set
     */
    public void setMatriculaSape(String matriculaSape) {
        this.matriculaSape = matriculaSape;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the natRendimento
     */
    public String getNatRendimento() {
        return natRendimento;
    }

    /**
     * @param natRendimento the natRendimento to set
     */
    public void setNatRendimento(String natRendimento) {
        this.natRendimento = natRendimento;
    }

    /**
     * @return the numPens
     */
    public String getNumPens() {
        return numPens;
    }

    /**
     * @param numPens the numPens to set
     */
    public void setNumPens(String numPens) {
        this.numPens = numPens;
    }

    /**
     * @return the renda
     */
    public BigDecimal getRenda() {
        return renda;
    }

    /**
     * @param renda the renda to set
     */
    public void setRenda(BigDecimal renda) {
        this.renda = renda;
    }

    /**
     * @return the previdenciaOficial
     */
    public BigDecimal getPrevidenciaOficial() {
        return previdenciaOficial;
    }

    /**
     * @param previdenciaOficial the previdenciaOficial to set
     */
    public void setPrevidenciaOficial(BigDecimal previdenciaOficial) {
        this.previdenciaOficial = previdenciaOficial;
    }

    /**
     * @return the previdenciaPrivada
     */
    public BigDecimal getPrevidenciaPrivada() {
        return previdenciaPrivada;
    }

    /**
     * @param previdenciaPrivada the previdenciaPrivada to set
     */
    public void setPrevidenciaPrivada(BigDecimal previdenciaPrivada) {
        this.previdenciaPrivada = previdenciaPrivada;
    }

    /**
     * @return the pensaoAlim
     */
    public BigDecimal getPensaoAlim() {
        return pensaoAlim;
    }

    /**
     * @param pensaoAlim the pensaoAlim to set
     */
    public void setPensaoAlim(BigDecimal pensaoAlim) {
        this.pensaoAlim = pensaoAlim;
    }

    /**
     * @return the retido
     */
    public BigDecimal getRetido() {
        return retido;
    }

    /**
     * @param retido the retido to set
     */
    public void setRetido(BigDecimal retido) {
        this.retido = retido;
    }

    /**
     * @return the proventosAposent
     */
    public BigDecimal getProventosAposent() {
        return proventosAposent;
    }

    /**
     * @param proventosAposent the proventosAposent to set
     */
    public void setProventosAposent(BigDecimal proventosAposent) {
        this.proventosAposent = proventosAposent;
    }

    /**
     * @return the diarias
     */
    public BigDecimal getDiarias() {
        return diarias;
    }

    /**
     * @param diarias the diarias to set
     */
    public void setDiarias(BigDecimal diarias) {
        this.diarias = diarias;
    }

    /**
     * @return the invalidez
     */
    public BigDecimal getInvalidez() {
        return invalidez;
    }

    /**
     * @param invalidez the invalidez to set
     */
    public void setInvalidez(BigDecimal invalidez) {
        this.invalidez = invalidez;
    }

    /**
     * @return the lucroPJ
     */
    public BigDecimal getLucroPJ() {
        return lucroPJ;
    }

    /**
     * @param lucroPJ the lucroPJ to set
     */
    public void setLucroPJ(BigDecimal lucroPJ) {
        this.lucroPJ = lucroPJ;
    }

    /**
     * @return the titularME
     */
    public BigDecimal getTitularME() {
        return titularME;
    }

    /**
     * @param titularME the titularME to set
     */
    public void setTitularME(BigDecimal titularME) {
        this.titularME = titularME;
    }

    /**
     * @return the indenizacao
     */
    public BigDecimal getIndenizacao() {
        return indenizacao;
    }

    /**
     * @param indenizacao the indenizacao to set
     */
    public void setIndenizacao(BigDecimal indenizacao) {
        this.indenizacao = indenizacao;
    }

    /**
     * @return the outrosIsentos
     */
    public String getOutrosIsentos() {
        return outrosIsentos;
    }

    /**
     * @param outrosIsentos the outrosIsentos to set
     */
    public void setOutrosIsentos(String outrosIsentos) {
        this.outrosIsentos = outrosIsentos;
    }

    /**
     * @return the outrosRendIsentos
     */
    public BigDecimal getOutrosRendIsentos() {
        return outrosRendIsentos;
    }

    /**
     * @param outrosRendIsentos the outrosRendIsentos to set
     */
    public void setOutrosRendIsentos(BigDecimal outrosRendIsentos) {
        this.outrosRendIsentos = outrosRendIsentos;
    }

    /**
     * @return the salario13
     */
    public BigDecimal getSalario13() {
        return salario13;
    }

    /**
     * @param salario13 the salario13 to set
     */
    public void setSalario13(BigDecimal salario13) {
        this.salario13 = salario13;
    }

    /**
     * @return the outrosRendExcl
     */
    public BigDecimal getOutrosRendExcl() {
        return outrosRendExcl;
    }

    /**
     * @param outrosRendExcl the outrosRendExcl to set
     */
    public void setOutrosRendExcl(BigDecimal outrosRendExcl) {
        this.outrosRendExcl = outrosRendExcl;
    }

    /**
     * @return the outrosExclusivo
     */
    public String getOutrosExclusivo() {
        return outrosExclusivo;
    }

    /**
     * @param outrosExclusivo the outrosExclusivo to set
     */
    public void setOutrosExclusivo(String outrosExclusivo) {
        this.outrosExclusivo = outrosExclusivo;
    }

    /**
     * @return the numProcessoRRA
     */
    public String getNumProcessoRRA() {
        return numProcessoRRA;
    }

    /**
     * @param numProcessoRRA the numProcessoRRA to set
     */
    public void setNumProcessoRRA(String numProcessoRRA) {
        this.numProcessoRRA = numProcessoRRA;
    }

    /**
     * @return the qtdMesesRRA
     */
    public String getQtdMesesRRA() {
        return qtdMesesRRA;
    }

    /**
     * @param qtdMesesRRA the qtdMesesRRA to set
     */
    public void setQtdMesesRRA(String qtdMesesRRA) {
        this.qtdMesesRRA = qtdMesesRRA;
    }

    /**
     * @return the natRendimentoRRA
     */
    public String getNatRendimentoRRA() {
        return natRendimentoRRA;
    }

    /**
     * @param natRendimentoRRA the natRendimentoRRA to set
     */
    public void setNatRendimentoRRA(String natRendimentoRRA) {
        this.natRendimentoRRA = natRendimentoRRA;
    }

    /**
     * @return the rendaRRA
     */
    public BigDecimal getRendaRRA() {
        return rendaRRA;
    }

    /**
     * @param rendaRRA the rendaRRA to set
     */
    public void setRendaRRA(BigDecimal rendaRRA) {
        this.rendaRRA = rendaRRA;
    }

    /**
     * @return the custasProcessuaisRRA
     */
    public BigDecimal getCustasProcessuaisRRA() {
        return custasProcessuaisRRA;
    }

    /**
     * @param custasProcessuaisRRA the custasProcessuaisRRA to set
     */
    public void setCustasProcessuaisRRA(BigDecimal custasProcessuaisRRA) {
        this.custasProcessuaisRRA = custasProcessuaisRRA;
    }

    /**
     * @return the deducaoPrevOficialRRA
     */
    public BigDecimal getDeducaoPrevOficialRRA() {
        return deducaoPrevOficialRRA;
    }

    /**
     * @param deducaoPrevOficialRRA the deducaoPrevOficialRRA to set
     */
    public void setDeducaoPrevOficialRRA(BigDecimal deducaoPrevOficialRRA) {
        this.deducaoPrevOficialRRA = deducaoPrevOficialRRA;
    }

    /**
     * @return the pensaoAlimenticiaRRA
     */
    public BigDecimal getPensaoAlimenticiaRRA() {
        return pensaoAlimenticiaRRA;
    }

    /**
     * @param pensaoAlimenticiaRRA the pensaoAlimenticiaRRA to set
     */
    public void setPensaoAlimenticiaRRA(BigDecimal pensaoAlimenticiaRRA) {
        this.pensaoAlimenticiaRRA = pensaoAlimenticiaRRA;
    }

    /**
     * @return the retidoRRA
     */
    public BigDecimal getRetidoRRA() {
        return retidoRRA;
    }

    /**
     * @param retidoRRA the retidoRRA to set
     */
    public void setRetidoRRA(BigDecimal retidoRRA) {
        this.retidoRRA = retidoRRA;
    }

    /**
     * @return the isentoMolestiaRRA
     */
    public BigDecimal getIsentoMolestiaRRA() {
        return isentoMolestiaRRA;
    }

    /**
     * @param isentoMolestiaRRA the isentoMolestiaRRA to set
     */
    public void setIsentoMolestiaRRA(BigDecimal isentoMolestiaRRA) {
        this.isentoMolestiaRRA = isentoMolestiaRRA;
    }

    /**
     * @return the infoCompl
     */
    public String getInfoCompl() {
        return infoCompl;
    }

    /**
     * @param infoCompl the infoCompl to set
     */
    public void setInfoCompl(String infoCompl) {
        this.infoCompl = infoCompl;
    }

    /**
     * @return the dtEmissao
     */
    public String getDtEmissao() {
        return dtEmissao;
    }

    /**
     * @param dtEmissao the dtEmissao to set
     */
    public void setDtEmissao(String dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    /**
     * @return the vlrRetido13s
     */
    public String getVlrRetido13s() {
        return vlrRetido13s;
    }

    /**
     * @param vlrRetido13s the vlrRetido13s to set
     */
    public void setVlrRetido13s(String vlrRetido13s) {
        this.vlrRetido13s = vlrRetido13s;
    }
    
    public InformeRendimentos obterInformeRendimentos(int codigoInformeRendimentos) throws Exception {
        InformeRendimentos informe = new InformeRendimentos();
        OracleDAL con = new OracleDAL();
        
        /*
        informe.setNumInfRendimento(123456);
        informe.setAnoBase(2016);
        informe.setEmpCodigo(155);
        informe.setSubEmpCodigo(1552);
        informe.setNome("João Otávio");
        informe.setCpf("06267152618");
        */
        
        con.connect();
        
        try {
            
            ResultSet rs = con.executarQuery("SELECT NUM_INF_RENDIMENTO, ANOBASE, EMP_CODIGO, SUBEMP_CODIGO, CGC, " +
                                             "       CNPJ, RAZAO, SUBEMP_RAZAO, NUMFUNC, NUMVINC, DESC_VINCULO, " +
                                             "       MATRICULA_SAPE, CPF, NOME, NAT_RENDIMETNO, NUMPENS, RENDA, " +
                                             "       PREVIDENCIA_OFICIAL, PREVIDENCIA_PRIVADA, PENSAO_ALIM, RETIDO, " +
                                             "       PROVENTOS_APOSENT, DIARIAS, INVALIDEZ, LUCRO_PJ, TITULAR_ME, " +
                                             "       INDENIZACAO, OUTROS_ISENTOS, OUTROS_REND_ISENTOS, SALARIO_13, " +
                                             "       OUTROS_REND_EXCL, OUTROS_EXCLUSIVO, NUM_PROCESSO_RRA, " +
                                             "       QTD_MESES_RRA, NAT_RENDIMENTO_RRA, RENDA_RRA, " +
                                             "       CUSTAS_PROCESSUAIS_RRA, DEDUCAO_PREV_OFICIAL_RRA, PENSAO_ALIMENTICIA_RRA, " +
                                             "       RETIDO_RRA, ISENTO_MOLESTIA_RRA, INFO_COMPL, DT_EMISSAO, VLR_RETIDO_13S " +
                                             "FROM PORTAL_ADM.INFORME_RENDIMENTOS WHERE NUM_INF_RENDIMENTO = " + Integer.toString(codigoInformeRendimentos));
            
            while (rs.next()) {          
                informe.setNumInfRendimento(rs.getInt("NUM_INF_RENDIMENTO"));
                informe.setAnoBase(rs.getInt("ANOBASE"));
                informe.setEmpCodigo(rs.getInt("EMP_CODIGO"));
                informe.setSubEmpCodigo(rs.getInt("SUBEMP_CODIGO"));
                informe.setCgc(rs.getString("CGC"));
                informe.setCnpj(rs.getString("CNPJ"));
                informe.setRazao(rs.getString("RAZAO"));
                informe.setSubEmpRazao(rs.getString("SUBEMP_RAZAO"));
                informe.setNumFunc(rs.getString("NUMFUNC"));
                informe.setNumVinc(rs.getString("NUMVINC"));
                informe.setDescVinculo(rs.getString("DESC_VINCULO"));
                informe.setMatriculaSape(rs.getString("MATRICULA_SAPE"));
                informe.setCpf(rs.getString("CPF"));
                informe.setNome(rs.getString("NOME"));
                informe.setNatRendimento(rs.getString("NAT_RENDIMETNO"));
                informe.setNumPens(rs.getString("NUMPENS"));
                informe.setRenda(rs.getBigDecimal("RENDA"));
                informe.setPrevidenciaOficial(rs.getBigDecimal("PREVIDENCIA_OFICIAL"));
                informe.setPrevidenciaPrivada(rs.getBigDecimal("PREVIDENCIA_PRIVADA"));
                informe.setPensaoAlim(rs.getBigDecimal("PENSAO_ALIM"));
                informe.setRetido(rs.getBigDecimal("RETIDO"));
                informe.setProventosAposent(rs.getBigDecimal("PROVENTOS_APOSENT"));
                informe.setDiarias(rs.getBigDecimal("DIARIAS"));
                informe.setInvalidez(rs.getBigDecimal("INVALIDEZ"));
                informe.setLucroPJ(rs.getBigDecimal("LUCRO_PJ"));
                informe.setTitularME(rs.getBigDecimal("TITULAR_ME"));
                informe.setIndenizacao(rs.getBigDecimal("INDENIZACAO"));
                informe.setOutrosIsentos(rs.getString("OUTROS_ISENTOS"));
                informe.setOutrosRendIsentos(rs.getBigDecimal("OUTROS_REND_ISENTOS"));
                informe.setSalario13(rs.getBigDecimal("SALARIO_13"));
                informe.setOutrosRendExcl(rs.getBigDecimal("OUTROS_REND_EXCL"));
                informe.setOutrosExclusivo(rs.getString("OUTROS_EXCLUSIVO"));
                informe.setNumProcessoRRA(rs.getString("NUM_PROCESSO_RRA"));                
                informe.setQtdMesesRRA(rs.getString("QTD_MESES_RRA"));
                informe.setNatRendimentoRRA(rs.getString("NAT_RENDIMENTO_RRA"));
                informe.setRendaRRA(rs.getBigDecimal("RENDA_RRA"));
                informe.setCustasProcessuaisRRA(rs.getBigDecimal("CUSTAS_PROCESSUAIS_RRA"));
                informe.setDeducaoPrevOficialRRA(rs.getBigDecimal("DEDUCAO_PREV_OFICIAL_RRA"));
                informe.setPensaoAlimenticiaRRA(rs.getBigDecimal("PENSAO_ALIMENTICIA_RRA"));
                informe.setRetidoRRA(rs.getBigDecimal("RETIDO_RRA"));
                informe.setIsentoMolestiaRRA(rs.getBigDecimal("ISENTO_MOLESTIA_RRA"));
                informe.setInfoCompl(rs.getString("INFO_COMPL"));
                informe.setDtEmissao(rs.getString("DT_EMISSAO"));
                informe.setVlrRetido13s(rs.getString("VLR_RETIDO_13S"));
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (con.isConnected())
                con.disconnect();

        }
        
        return informe;
    }
}