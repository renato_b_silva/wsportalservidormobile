/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.rj.seplag.model;

/**
 *
 * @author joao.dias
 */
public class RetornoInformeRendimentos extends Retorno {
    
     private InformeRendimentos infoRendimentos;
    
    public InformeRendimentos getInformeRendimentos() {
        return infoRendimentos;
    };
    
    public void setInformeRendimentos(InformeRendimentos infoRendimentos) {
        this.infoRendimentos = infoRendimentos;
    };
}
