package br.gov.rj.seplag.config;

import javax.inject.Inject;
import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import br.gov.rj.seplag.ws.portalservidor.PortalServidorMobileServiceImpl;

@Configuration
@ImportResource({"classpath:META-INF/cxf/cxf.xml", "classpath:META-INF/cxf/cxf-servlet.xml"})
public class CxfConfiguration {

	@Inject
	private Bus bus;

    @Bean
    public PortalServidorMobileServiceImpl portalServidorMobileService() {
        return new PortalServidorMobileServiceImpl();
    }

    @Bean
    public Endpoint portalServidorMobileServiceEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, portalServidorMobileService());
        endpoint.publish("/PortalServidorMobile");
        return endpoint;
    }

}
